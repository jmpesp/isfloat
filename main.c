#include <stdio.h>
enum {S0 = 0, S1, S2, S3, S4, T, F};
char stt[4][4] = {
	{S2, S1, F, F},
	{S2, S1, T, F},
	{F , S3, F, F},
	{F , S3, T, F}
};

char x(char c) {
	switch(c) {
		case '.': return 0;
		case '\0': return 2;
		default: if(c>='0' && c<='9') return 1;
	}
	return 3;
}
int main() {
	char buf[100] = "0.2333";
	char *p = buf, status = S0;
	while(status<T) {
		status = stt[status][x(*p)];
		p++;
	}
	printf("%s", status == T ? "true" : "false");
	return 0;
}
